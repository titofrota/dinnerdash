class SituationsController < ApplicationController

    def index
        @situation = Situation.all 
        render json: @situation, status: 200
    end

    def create
        @situation = Situation.new(description: params[:situation])
        if @situation.save
            render json: @situation, status: 200
        else
            render json: @situation.errors
        end
    end

    def update
        @situation = Situation.find(params[:id])
        @situation = @situation.update(description: params[:situation])
        render json: @situation, status: 200
    end

    def show
        @situation = Situation.find(params[:id])
        render json: @situation, status: 200
    end

    def delete
        @situation = Situation.find(params[:id])
        @situation.delete
    end


end
