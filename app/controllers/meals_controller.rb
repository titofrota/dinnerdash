class MealsController < ApplicationController

    def index
        @meals = Meal.all 
        render json: @meals, status: 200
    end

    def show
        @meal = Meal.find(params[:id])
        render json: @meal, status: 200
    end

    def create
        @meal = Meal.new(name: params[:name], description: params[:description], price: params[:price], image: params[:image], available: params[:available], meal_category_id: params[:categoryid])
        if @meal.save
            render json: @meal, status: 200
        else
            render json: @meal.errors
        end
    end

    def update
        @meal = Meal.find(params[:id])
        @meal = @meal.update(name: params[:name], description: params[:description], price: params[:price], image: params[:image], available: params[:available], meal_category_id: params[:categoryid])
        render json: @meal, status: 200
    end

    def delete
        @meal = Meal.find(params[:id])
        @meal.delete
    end
    

end
