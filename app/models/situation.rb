class Situation < ApplicationRecord
    has_many :orders
    validates :description, presence: true
    validates :description, length: { maximum: 45 }
end
