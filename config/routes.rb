Rails.application.routes.draw do
  resources :situations
  resources :orders
  resources :order_has_meals
  #resources :#
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.
  
#MEALCATEGORY#
  get "/mealcategory/index" => "meal_categories#index"
  get "/mealcategory/:id" => "meal_categories#show"
  post "/mealcategory/create" => "meal_categories#create"
  put "/mealcategory/:id" => "meal_categories#update"
  delete "/mealcategory/:id" => "meal_categories#delete"

#MEAL#
  get "/meal/index" => "meals#index"
  get "/meal/:id" => "meals#show"
  post "/meal/create" => "meals#create"
  put "/meal/:id" => "meals#update"
  delete "/meal/:id" => "meals#delete"

#ORDERHASMEAL#
  post "/ordermeal/create" => "order_has_meals#create"
  put "/ordermeal/:id" => "order_has_meals#update"
  delete "/ordermeal/:id" => "order_has_meals#delete"

#ORDER#
  get "/order/index" => "orders#index"
  get "/order/:id" => "orders#show"
  post "/order/create" => "orders#create"
  put "/order/:id" => "orders#update"
  delete "/order/:id" => "orders#delete"

#SITUATION#
  get "/situation/index" => "situations#index"
  get "/situation/:id" => "situations#show"
  post "/situation/create" => "situations#create"
  put "/situation/:id" => "situations#update"
  delete "/situation/:id" => "situations#delete"

end
